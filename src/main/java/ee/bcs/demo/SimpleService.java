package ee.bcs.demo;

public class SimpleService 
{
	public int add5(int number) {
		return number + 5;
	}
	
	public String concatenateStrings(String string1, String string2) {
		return string1 + string2;
	}
}
