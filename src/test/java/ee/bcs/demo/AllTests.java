package ee.bcs.demo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SimpleServiceNumbersTest.class, SimpleServiceStringsTest.class })
public class AllTests {

}
