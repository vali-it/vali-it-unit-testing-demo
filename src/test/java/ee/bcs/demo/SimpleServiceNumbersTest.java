package ee.bcs.demo;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleServiceNumbersTest {

	@Test
	public void testAdd5() {
		SimpleService simpleService = new SimpleService();
		int result = simpleService.add5(5);
		assertEquals(10, result);
	}
}
