package ee.bcs.demo;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleServiceStringsTest {

	@Test
	public void testConcatenateStrings() {
		SimpleService simpleService = new SimpleService();
		String result = simpleService.concatenateStrings("Kapa", "Kohila");
		assertEquals("KapaKohila", result);
	}
}
